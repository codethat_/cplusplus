#include <iostream>
#include <string>

using namespace std;

//ENCAPSULATION = combine what an object is with what it does
/*-------------------------------------------------------------------------------*/
class Monster
{
	public:
		//Constructor
		Monster() {cout << "\n\tBuilding a Monster...";}
		//Destructor
		~Monster() {cout << "\n\tDestroying a Monster...";}

	//Member Methods
	void Rampage() {cout << "\n\tMonster rampagign through the city!";}

	void DisplayStats()
	{
		cout << "\n\n\t-------Monster Stats--------";
		cout << "\n\tName: " << MonsterName;
		cout << "\n\tLife: " << LIFE;
		cout << "\n\tSize: " << SIZE;
		cout << "\n\tWeight: "  << WEIGHT;
		cout << "\n\t----------------------------";

	}

	//Accessor Methods
	int GetLife() {return LIFE;}
	void SetLife(int x) {LIFE = x;}
	double GetSize() {return SIZE;}
	void SetSize(int x) {SIZE = x;}
	double GetWeight() {return WEIGHT;}
	void SetWeight(double x) {WEIGHT = x;}
	string GetMonsterName() {return MonsterName;}
	void SetMonsterName(string x) {MonsterName = x;}

	private:
		int LIFE;
		double SIZE;
		double WEIGHT;
		string MonsterName;
};

/*-------------------------------------------------------------------------------*/

class Dragon : public Monster
{
	public:
		Dragon() {cout << "\n\tBuilding a dragon";}
		~Dragon() {cout << "\n\tDestroying a Dragon";}

		void BreathFire() {cout << "\n\tDragon breathing fire\n";}
};

/*-------------------------------------------------------------------------------*/

class Unicorn : public Monster
{
	public:
		Unicorn() {cout << "\n\tBuilding a Unicorn";}
		~Unicorn() {cout << "\n\tDestroying a Unicorn";}

		void Fly() {cout << "\n\tUnicorn flying\n";}
};

/*-------------------------------------------------------------------------------*/

int main()
{
	//stack
	int x = 10;
	double y = 34.567;
	char LETTER = 'A';
	char MyString[10] = "Testing";
	string DATA1;

	Dragon * Bill = new Dragon();
	Unicorn * Sue = new Unicorn();

	Bill->SetMonsterName("Bill");
	Bill->SetLife(500);
	Bill->SetSize(50);
	Bill->SetWeight(4);
	Bill->DisplayStats();
	Bill->BreathFire();

	Sue->SetMonsterName("Sue");
	Sue->SetLife(350);
	Sue->SetSize(25);
	Sue->SetWeight(2);
	Sue->DisplayStats();
	Sue->Fly();

	cout << "\n\n\t";

	return 0;
}
